
pipeline {
agent {
        kubernetes {
      yaml """
apiVersion: v1
kind: Pod
metadata:
  labels:
    some-label: test-odu
spec:
  securityContext:
    runAsUser: 10000
    runAsGroup: 10000
  containers:
  - name: jnlp
    image: 'jenkins/jnlp-slave:4.3-4-alpine'
    args: ['\$(JENKINS_SECRET)', '\$(JENKINS_NAME)']
  - name: yair
    image: us.icr.io/dc-tools/security/yair:1
    command:
    - cat
    tty: true
    imagePullPolicy: Always
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug-1534f90c9330d40486136b2997e7972a79a69baf
    imagePullPolicy: Always
    command:
    - cat
    tty: true   
    securityContext: # https://github.com/GoogleContainerTools/kaniko/issues/681
      runAsUser: 0
      runAsGroup: 0
  - name: openshift-cli
    image: openshift/origin-cli:v3.11.0
    command:
    - cat
    tty: true
    securityContext: # https://github.com/GoogleContainerTools/kaniko/issues/681
      runAsUser: 0
      runAsGroup: 0
  volumes:
  - name: regsecret
    projected:
      sources:
      - secret:
          name: regsecret
          items:
            - key: .dockerconfigjson
              path: config.json
  imagePullSecrets:
  - name: oduregsecret
  - name: regsecret
"""
        }
    }
  environment {
    
     /* -----------DevOps Commander  created env variables------------ */
     /*===============================================================*/
	
	DOCKER_URL= "us.icr.io/dc-tools"
DOCKER_CREDENTIAL_ID= "dc-docker-3929"
BITBUCKET_URL= "https://bitbucket.org"
BITBUCKET_CREDENTIAL_ID= "dc-bitbucket-3929"
SONARQUBE_URL= "https://sonarqube-1749-3725.dc-ig-lib-ga-1589529604-f72ef11f3ab089a8c677044eb28292cd-0000.au-syd.containers.appdomain.cloud"
SONARQUBE_CREDENTIAL_ID= "dc-sonarqube-3929"
OCP_URL= "https://c100-e.au-syd.containers.cloud.ibm.com:31995"
OCP_CREDENTIAL_ID= "dc-ocp-3929-dc-apps"
NAMESPACE= "dc-apps"
INGRESS= "dc-ig-lib-ga-1589529604-f72ef11f3ab089a8c677044eb28292cd-0000.au-syd.containers.appdomain.cloud"
	
	/* -----------DevOps Commander  created env variables------------ */

          VERSION="1-SNAPSHOT"

        /* Parameters for Docker image that will be built and deployed */
        /* REGISTRY_USERNAME provided via a Jenkins secret
         * REGISTRY_PASSWORD provided via a Jenkins secret
         */
        DOCKER_IMAGE="${JOB_BASE_NAME}".toLowerCase()
	      DEPLOYMENT_NAME="${JOB_BASE_NAME}".toLowerCase()
        DOCKER_TAG="dev"
        K8S_DEPLOYMENT="spring-java-test"
        componentName = readMavenPom().getArtifactId()
        componentVersion = readMavenPom().getVersion()
  }
 
  stages {
        stage ('Develop: Checkout') {
            steps {
                withMaven(
                    maven: 'maven-3',
     //               mavenSettingsConfig: 'java-dc',
                   mavenLocalRepo: '.repository'
                ) {
                    /* The reports dir is used to store outputs of DependencyCheck, Clair and ZAP analysis */
                    sh '''
                        mvn clean
                        mkdir -p reports
                    '''
                }
            }
        }
        stage ('Build: Maven') {
            steps {
                withMaven(
                    maven: 'maven-3',
           //        mavenSettingsConfig: 'java-dc',
                    mavenLocalRepo: '.repository'
                ) {
                    sh 'mvn compile'
                }
            }
        }


        stage ('Build: Application Package') {
            steps {
                withMaven(
                    maven: 'maven-3',
            //        mavenSettingsConfig: 'java-dc',
                    mavenLocalRepo: '.repository'
                ) {
                    sh 'mvn package'
                    sh 'mv target/${componentName}-${componentVersion}.jar target/app.jar'
                }
            }
        }
		       stage('SonarQube Code Analysis') {  
	   when {
               beforeOptions true
               expression { env.SONARQUBE_URL }
           }
           steps {
             script{ 
                   withMaven(
                   maven: 'maven-3',
	           mavenLocalRepo: '.repository'
		   ){
                   sh '''
		      mkdir .sonar .sonar/cache .scannerwork
                      chmod -R 777 .sonar
                      chmod -R 777 .scannerwork
		      export SONAR_USER_HOME=${WORKSPACE}
		      mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.3.0.603:sonar \
		      -f pom.xml \
		      -Dsonar.host.url=$SONARQUBE_URL  \
		      -Dsonar.projectKey=${NAMESPACE}-${DEPLOYMENT_NAME} \
		      -Dsonar.projectName=${NAMESPACE}-${DEPLOYMENT_NAME} \
		      -Dsonar.language=java \
		      -Dsonar.sources=. \
		      -Dsonar.java.binaries=target/classes \
		      -Dsonar.tests=. \
		      -Dsonar.test.inclusions=**/*Test*/* \
		      -Dsonar.exclusions=target/**/*.class
		   '''
		   }
                 }
             }
        }


stage("Quality Gate") {
  when {
               beforeOptions true
               expression { env.SONARQUBE_URL }
           }
            steps {
		     container('openshift-cli') {
		     withCredentials([usernamePassword(credentialsId: "${SONARQUBE_CREDENTIAL_ID}", usernameVariable: 'SONARQUBE_USER', passwordVariable: 'SONARQUBE_TOKEN')]){
                    sh '''
		    curl -u ${SONARQUBE_TOKEN}: ${SONARQUBE_URL}/api/qualitygates/project_status?projectKey=${NAMESPACE}-${DEPLOYMENT_NAME} -s > SONAR_RESPONSE
		    cat SONAR_RESPONSE
#		    grep "ERROR" SONAR_RESPONSE
		    RC=`grep "ERROR" SONAR_RESPONSE` || true
		    echo "RC=$RC ."
		    if [ -z "$RC" ]
		    then
  			echo "QG Passed"
		    else
		        echo "QG Failed"
			exit 1
		    fi
		    echo "QG checked"	
		    '''
		     }
		    }
            }
        }	






         stage ('Build: Docker') {
            steps {
                container('kaniko') {
	                   withCredentials([usernamePassword(credentialsId: "${DOCKER_CREDENTIAL_ID}", usernameVariable: 'REGISTRY_USERNAME', passwordVariable: 'REGISTRY_PASSWORD')]) {
                    /* Kaniko uses secret 'regsecret' declared in the POD to authenticate to the registry and push the image */
                    sh '''
                    mkdir -p /kaniko/.docker
                    echo '{\"auths\":{\"'${DOCKER_URL}'\":{\"username\":\"'${REGISTRY_USERNAME}'\",\"password\":\"'${REGISTRY_PASSWORD}'\"}}}' > /kaniko/.docker/config.json
			              cat /kaniko/.docker/config.json
		                /kaniko/executor -f `pwd`/Dockerfile -c `pwd` --insecure --skip-tls-verify --cache=true --destination=${DOCKER_URL}/${DOCKER_IMAGE}:${DOCKER_TAG}
		               '''
                     }
	               }
              }
            }
     		

        stage('Deploy: To Openshift') {
        steps {
          container('openshift-cli') {
	     withCredentials([
	        usernamePassword(credentialsId: "${OCP_CREDENTIAL_ID}", usernameVariable: 'REGISTRY_USERNAME', passwordVariable: 'TOKEN'),
		      usernamePassword(credentialsId: "${DOCKER_CREDENTIAL_ID}", usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')
	     ]) {
              sh '''
              oc login --server="${OCP_URL}" --token="${TOKEN}"
              oc project ${NAMESPACE}
              pwd
              ls -ltr
              oc create secret docker-registry docker-repo-cred \
              --docker-server=${DOCKER_URL} \
              --docker-username=${DOCKER_USERNAME} \
              --docker-password=${DOCKER_PASSWORD} \
              --docker-email=${DOCKER_PASSWORD} \
              --namespace=${NAMESPACE} \
              || true
              sed -e "s~{REGISTRY_NAME}~$DOCKER_URL~g" \
                  -e "s~{DOCKER_IMAGE}~$DOCKER_IMAGE~g" \
                  -e "s~{DOCKER_TAG}~$DOCKER_TAG~g" \
                  -e "s~{K8S_DEPLOYMENT}~$DEPLOYMENT_NAME~g" \
                  -e "s~{INGRESS_URL}~$INGRESS~g" -i devops/k8s/*.yml
              oc apply -f devops/k8s/ --namespace="${NAMESPACE}" \
              || true
              oc create route edge --service=${DEPLOYMENT_NAME}-svc --hostname=$DEPLOYMENT_NAME.$INGRESS ||true
              oc wait --for=condition=available --timeout=120s deployment/${DEPLOYMENT_NAME} --namespace="${NAMESPACE}" \
              || true
              '''
	     }
           }
         }
        }




    }
}

